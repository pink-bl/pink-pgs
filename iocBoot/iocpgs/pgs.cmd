#!../../bin/linux-x86_64/pgs

## You may have to change pgs to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/pgs.dbd"
pgs_registerRecordDeviceDriver pdbbase

## test for docker-compose
#epicsEnvSet("IOCBL", "PINK")
#epicsEnvSet("IOCDEV", "PG01")
#epicsEnvSet("BPM", "BPM1")
#epicsEnvSet("CAMERA_ID", "16435513")

# Prefix for all records
epicsEnvSet("BL", "$(IOCBL)")
epicsEnvSet("DEV", "$(IOCDEV)")
epicsEnvSet("PREFIX","$(BL):$(DEV):")

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", 10000000)

# Use this line for a specific camera by serial number, in this case a BlackFly GigE camera
## Camera PG01
#epicsEnvSet("CAMERA_ID", "16435513")
## Camera PG02
## epicsEnvSet("CAMERA_ID", "16435512")
## Camera PG03
## epicsEnvSet("CAMERA_ID", "16435509")
## Camera PG04
## epicsEnvSet("CAMERA_ID", "15395012")

# The port name for the detector
epicsEnvSet("PORT",   "PGP1")
# Really large queue so we can stream to disk at full camera speed
epicsEnvSet("QSIZE",  "2000")
# The maximim image width; used for row profiles in the NDPluginStats plugin
epicsEnvSet("XSIZE",  "1288")
# The maximim image height; used for column profiles in the NDPluginStats plugin
epicsEnvSet("YSIZE",  "964")
# The maximum number of time series points in the NDPluginStats plugin
epicsEnvSet("NCHANS", "2048")
# The maximum number of frames buffered in the NDPluginCircularBuff plugin
epicsEnvSet("CBUFFS", "500")
# The search path for database files
epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(ADCORE)/db")
# Define NELEMENTS to be enough for a 1288x964 = 1241632 (Mono) image
epicsEnvSet("NELEMENTS", "1241632")

# pointGreyConfig(const char *portName, int cameraId, int traceMask, int memoryChannel, int maxBuffers, size_t maxMemory, int priority, int stackSize)
pointGreyConfig("$(PORT)", $(CAMERA_ID), 0x1, 0)

## test
#drvAsynIPPortConfigure("$(PORT)", "127.0.0.1:50123", 0, 0, 0)

dbLoadRecords("$(ADPOINTGREY)/db/pointGrey.template", "P=$(BL):,R=$(DEV):,PORT=$(PORT),ADDR=0,TIMEOUT=1")
dbLoadTemplate("${TOP}/iocBoot/${IOC}/pgs.substitutions", "P=$(BL):,R=$(DEV):,PORT=$(PORT)" )

# Create a standard arrays plugin
NDStdArraysConfigure("Image1", 5, 0, "$(PORT)", 0, 0)
NDStdArraysConfigure("Image2", 5, 0, "$(PORT)", 0, 0)

# Use this line for 8-bit or 16-bit data
dbLoadRecords("$(ADCORE)/db/NDStdArrays.template", "P=$(PREFIX),R=image1:,PORT=Image1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int16,FTVL=USHORT,NELEMENTS=$(NELEMENTS)")
dbLoadRecords("$(ADCORE)/db/NDStdArrays.template", "P=$(PREFIX),R=image2:,PORT=Image2,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int16,FTVL=USHORT,NELEMENTS=$(NELEMENTS)")

cd "${TOP}/iocBoot/${IOC}"

# Extra customized records for PINK Beamline
dbLoadRecords("${TOP}/iocBoot/${IOC}/extra.db", "BL=$(BL), DEV=$(DEV), BPM=$(BPM)")

# Load all other plugins using commonPlugins.cmd
< plugins.cmd

## Autosave Path and Restore settings
set_savefile_path("/EPICS/autosave")
set_requestfile_path("${TOP}/iocBoot/${IOC}/reqs")
set_requestfile_path("$(ADCORE)/ADApp/Db")
set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")

iocInit

## Autosave Monitor settings
create_monitor_set("auto_settings.req", 30,"BL=$(BL), DEV=$(DEV), P=$(PREFIX)")

# Records with dynamic enums need to be processed again because the enum values are not available during iocInit.
dbpf("$(BL):$(DEV):Format7Mode.PROC", "1")
dbpf("$(BL):$(DEV):PixelFormat.PROC", "1")

# Wait for callbacks on the property limits (DRVL, DRVH) to complete
epicsThreadSleep(1.0)

# Records that depend on the state of the dynamic enum records or property limits also need to be proce$
# Other property records may need to be added to this list
dbpf("$(BL):$(DEV):FrameRate.PROC", "1")
dbpf("$(BL):$(DEV):FrameRateValAbs.PROC", "1")
dbpf("$(BL):$(DEV):AcquireTime.PROC", "1")

epicsThreadSleep(1.0)
dbpf("$(BL):$(DEV):PixelFormat", "1")
dbpf("$(BL):$(DEV):ShutterAutoMode", "0")
dbpf("$(BL):$(DEV):GainAutoMode", "0")

## cross on over1:6 follows stat2 centroid
dbpf("$(BL):$(DEV):Over1:6:CenterXLink.DOL", "$(BL):$(DEV):Stats2:CentroidX_RBV CP MS")
dbpf("$(BL):$(DEV):Over1:6:CenterYLink.DOL", "$(BL):$(DEV):Stats2:CentroidY_RBV CP MS")
dbpf("$(BL):$(DEV):TIFF1:FilePath", "/EPICS/Pinkdata/BPM")
dbpf("$(BL):$(DEV):TIFF1:FileTemplate", "%s%s")
dbpf("$(BL):$(DEV):PacketDelay", "3000")


## Start any sequence programs
#seq sncxxx,"user=epics"
